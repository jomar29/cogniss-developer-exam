const console = require("console");
const fs = require("fs");

let nameData = fs.readFileSync("input2.json");
let rawNames = JSON.parse(nameData);
let names = rawNames.names;

let mail = {
  emails: [],
};

names.map((name) => {
  let rawEmail = name.substring(0, 5);
  let email = `${rawEmail.toLowerCase()}@gmail.com`;

  mail.emails.push(email);
});

///create a new json
let data = JSON.stringify(mail);

fs.writeFile("output2.json", data, { spaces: 2 }, function (err) {
  console.log("Saved!");
});
